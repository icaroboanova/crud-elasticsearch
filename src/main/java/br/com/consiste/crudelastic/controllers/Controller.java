package br.com.consiste.crudelastic.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;

import org.apache.http.HttpHost;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.consiste.crudelastic.util.Utils;



@RequestMapping("/elastic")
@RestController
@CrossOrigin
public class Controller {
	
	@GetMapping("/")
	public ResponseEntity<String> helloWorld(){
		return new ResponseEntity<String>("Hello World!", HttpStatus.OK);
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> createDocument(InputStream data){
		try {
			
			JsonObject body = Utils.getBody(data);
			
			// on startup
			RestHighLevelClient client = Utils.createClient();

			JsonObject documento = body.get("documento").getAsJsonObject();
			
			
			IndexRequest request = new IndexRequest(body.get("indice").getAsString(), "_doc");
			
			request.source(documento.toString(), XContentType.JSON);
			
			IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
			
			// on shutdown
			client.close();
			
			return new ResponseEntity<String>("Documento criado com sucesso!", HttpStatus.CREATED);
		}catch(Exception e) {
			System.out.println(e);
			return new ResponseEntity<String>("Parametros incorretos", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateDocument(InputStream data){
		try {
			JsonObject body = Utils.getBody(data);
			
			RestHighLevelClient client = Utils.createClient();
			
			UpdateRequest request = new UpdateRequest(
			        body.get("indice").getAsString(), 
			        "_doc",  
			        body.get("id").getAsString()); 
			
			request.doc(body.get("documento").getAsJsonObject().toString(), XContentType.JSON);
			
			UpdateResponse updateResponse = client.update(
			        request, RequestOptions.DEFAULT);
		
			System.out.println(updateResponse.getGetResult());
			
			return new ResponseEntity<String>("Documento editado com sucesso!", HttpStatus.CREATED);
		}catch(IOException e) {
			System.out.println(e);
			return new ResponseEntity<String>("Erro interno", HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}
	
	@DeleteMapping("/delete")
	public ResponseEntity<?> deleteDocument(InputStream data){
		try {
			JsonObject body = Utils.getBody(data);
			
			RestHighLevelClient client = Utils.createClient();
			
			DeleteRequest request = new DeleteRequest(
					body.get("indice").getAsString(),    
			        "_doc",      
			        body.get("id").getAsString());
			
			DeleteResponse deleteResponse = client.delete(
			        request, RequestOptions.DEFAULT);
			
			System.out.println(deleteResponse.getResult());
			
			return new ResponseEntity<String>("Documento excluido com sucesso!", HttpStatus.CREATED);
		}catch(IOException e) {
			System.out.println(e);
			return new ResponseEntity<String>("Erro interno", HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}
	
	@GetMapping("/alldocuments")
	public ResponseEntity<?> getAllDocuments(String indice){
		try {
			RestHighLevelClient client = Utils.createClient();
			
			SearchRequest searchRequest = new SearchRequest(indice);
			
			SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
			
			SearchHits hits = searchResponse.getHits();
			
			SearchHit[] searchHits = hits.getHits();
			
			JsonArray resposta = new JsonArray();
			JsonParser parser = new JsonParser();
			for (SearchHit hit : searchHits) {
				resposta.add(parser.parse(hit.getSourceAsString()));
			}
			
			return new ResponseEntity<String>(resposta.toString(), HttpStatus.CREATED);
		}catch(IOException e) {
			System.out.println(e);
			return new ResponseEntity<String>("Erro interno", HttpStatus.INTERNAL_SERVER_ERROR); 
		}
	}

}


