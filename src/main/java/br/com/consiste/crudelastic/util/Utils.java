package br.com.consiste.crudelastic.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Utils {
	
	public static JsonObject getBody(InputStream data) throws IOException {
		StringBuilder sb = new StringBuilder();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(data, "utf-8"));
		String line = null;

		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		
		JsonParser parse = new JsonParser();
		return parse.parse(sb.toString()).getAsJsonObject();
	}
	
	public static RestHighLevelClient createClient() {
		return new RestHighLevelClient(
		        RestClient.builder(
		                new HttpHost("xtr-verde.consiste.com.br", 9200, "http")));
	}

}
